package com.PhotoIdentifier3;

import android.content.Context;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import java.io.IOException;
import java.lang.reflect.Method;

class Preview extends SurfaceView implements Callback {
    private static final String TAG = "Preview";
    public static Camera camera;
    SurfaceHolder mHolder;
    private boolean previewing = false;
    int rotate = 90;

    Preview(Context context) {
        super(context);
        try {
            this.mHolder = getHolder();
            this.mHolder.addCallback(this);
            this.mHolder.setType(3);
        } catch (Exception e) {
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera = Camera.open();
            camera.setDisplayOrientation(this.rotate);
            try {
                camera.setPreviewDisplay(holder);
            } catch (Exception e) {
                camera.release();
                camera = null;
            }
        } catch (Exception e2) {
            System.out.println("Iam at e");
            e2.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (camera != null) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.previewing) {
            camera.stopPreview();
            this.previewing = false;
        }
        if (camera != null) {
            try {
                camera.setPreviewDisplay(this.mHolder);
                camera.startPreview();
                this.previewing = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setDisplayOrientation(Camera camera2, int angle) {
        try {
            Method downPolymorphic = camera2.getClass().getMethod("setDisplayOrientation", new Class[]{Integer.TYPE});
            if (downPolymorphic != null) {
                downPolymorphic.invoke(camera2, new Object[]{Integer.valueOf(angle)});
            }
        } catch (Exception e) {
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
