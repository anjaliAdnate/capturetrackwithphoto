package com.PhotoIdentifier3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;
import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.p003v1.XmlPullParser;

public class BinsActivity extends Activity implements Callback {
    /* access modifiers changed from: private */
    public static String OPERATION_NAME = null;
    /* access modifiers changed from: private */
    public static String SOAP_ACTION = null;
    private static final String SOAP_ADDRESS = "http://34.93.133.27:8181/vtsws/binlocationservice1.asmx";
    private static final String WSDL_TARGET_NAMESPACE = "http://www.ramky.com/";
    private static int hgt = 0;
    private static int wid = 0;
    String IMEIno = XmlPullParser.NO_NAMESPACE;
    Bitmap ImgBitmap = null;
    /* access modifiers changed from: private */
    public Camera camera = null;
    private SurfaceHolder cameraSurfaceHolder = null;
    private SurfaceView cameraSurfaceView = null;
    private Display display = null;
    final PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                BinsActivity.this.f0t = data;
                BinsActivity.this.currentLocation();
                new UploadInfo().execute(new String[0]);
            } catch (Exception e) {
            }
        }
    };
    double latitude = 0.0d;
    LocationManager locationManager;
    double longitude = 0.0d;
    LocationListener mlocListener;
    /* access modifiers changed from: private */
    public boolean previewing = false;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    List<String> providers;
    final PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };
    SoapObject request;
    Object result;
    SoapObject resultGet;
    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        }
    };
    String strBase64 = XmlPullParser.NO_NAMESPACE;

    /* renamed from: t */
    byte[] f0t;

    public class MyLocationListener implements LocationListener {
        public MyLocationListener() {
        }

        public void onLocationChanged(Location loc) {
            BinsActivity.this.latitude = loc.getLatitude();
            BinsActivity.this.longitude = loc.getLongitude();
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    class UploadInfo extends AsyncTask<String, String, String> {
        UploadInfo() {
        }

        public void onPreExecute() {
            BinsActivity.this.progressDialog = new ProgressDialog(BinsActivity.this);
            BinsActivity.this.progressDialog.setMessage("Please wait while uploading..");
            BinsActivity.this.progressDialog.setProgressStyle(0);
            BinsActivity.this.progressDialog.setCancelable(false);
            BinsActivity.this.progressDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            BinsActivity.SOAP_ACTION = "http://www.ramky.com/BinCapturing";
            BinsActivity.OPERATION_NAME = "BinCapturing";
            BinsActivity.this.request = new SoapObject(BinsActivity.WSDL_TARGET_NAMESPACE, BinsActivity.OPERATION_NAME);
            TelephonyManager telephonyManager = (TelephonyManager) BinsActivity.this.getSystemService("phone");
            BinsActivity.this.IMEIno = telephonyManager.getDeviceId();
            BinsActivity.this.request.addProperty("IME_number", (Object) BinsActivity.this.IMEIno.trim());
            BinsActivity.this.request.addProperty("Lat_value", (Object) Double.toString(BinsActivity.this.latitude));
            BinsActivity.this.request.addProperty("Lon_value", (Object) Double.toString(BinsActivity.this.longitude));
            BinsActivity.this.request.addProperty("BinImg", (Object) Base64.encode(BinsActivity.this.f0t));
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(BinsActivity.this.request);
            try {
                new HttpTransportSE(BinsActivity.SOAP_ADDRESS).call(BinsActivity.SOAP_ACTION, envelope);
                BinsActivity.this.result = envelope.getResponse();
                Log.i("request result", BinsActivity.this.result.toString());
            } catch (Exception exception) {
                exception.toString();
            }
            BinsActivity.this.progressDialog.dismiss();
            return null;
        }

        public void onPostExecute(String params) {
            BinsActivity.this.progressDialog.dismiss();
            try {
                String requestResult = BinsActivity.this.result.toString();
                if (requestResult.equalsIgnoreCase("ok")) {
                    Toast.makeText(BinsActivity.this, "Successfully uploaded ...", 1).show();
                    BinsActivity.this.camera.stopPreview();
                    BinsActivity.this.camera.release();
                    BinsActivity.this.camera = null;
                    BinsActivity.this.previewing = false;
                    BinsActivity.this.finish();
                    BinsActivity.this.startActivity(new Intent(BinsActivity.this, BinsActivity.class));
                    return;
                }
                Toast.makeText(BinsActivity.this, "Please try again..." + requestResult, 1).show();
            } catch (NullPointerException e) {
                Toast.makeText(BinsActivity.this, "Please check your internet connection..", 1).show();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, Newservice.class));
        setRequestedOrientation(0);
        this.display = getWindowManager().getDefaultDisplay();
        wid = this.display.getWidth();
        hgt = this.display.getHeight();
        getWindow().setFormat(-3);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        try {
            if (this.camera != null) {
                this.camera.stopPreview();
                this.camera.release();
                this.camera = null;
            }
            try {
                setContentView(C0033R.layout.homescreen);
                this.cameraSurfaceView = (SurfaceView) findViewById(C0033R.C0034id.cameraSurfaceView);
                this.cameraSurfaceHolder = this.cameraSurfaceView.getHolder();
                this.cameraSurfaceHolder.addCallback(this);
                this.cameraSurfaceHolder.setType(3);
            } catch (Exception t) {
                t.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.previewing) {
            this.camera.stopPreview();
            this.previewing = false;
        }
        if (this.camera != null) {
            try {
                this.camera.setPreviewDisplay(this.cameraSurfaceHolder);
                this.camera.startPreview();
                this.previewing = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.camera = Camera.open();
        } catch (RuntimeException e) {
            Toast.makeText(getApplicationContext(), "Device camera is not working properly, please try after sometime.", 1).show();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.out.println("PAUSEJAAJSJF");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onPause();
        System.out.println("****************");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (keyCode == 4) {
            finish();
        } else if (keyCode == 25) {
            this.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
        } else if (keyCode == 24) {
            this.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
        }
        return true;
    }

    public void currentLocation() {
        this.locationManager = (LocationManager) getSystemService("location");
        this.providers = this.locationManager.getProviders(true);
        this.mlocListener = new MyLocationListener();
        this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.mlocListener);
        System.out.println("providers size" + this.providers.size());
        for (int i = 0; i < this.providers.size(); i++) {
            System.out.println("providers " + ((String) this.providers.get(i)));
        }
        Location location = null;
        for (int i2 = this.providers.size() - 1; i2 >= 0; i2--) {
            location = this.locationManager.getLastKnownLocation((String) this.providers.get(i2));
            if (location != null) {
                break;
            }
        }
        if (location == null) {
            System.out.print("location null");
        }
        if (location != null) {
            this.latitude = 0.0d;
            this.longitude = 0.0d;
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }
    }
}
