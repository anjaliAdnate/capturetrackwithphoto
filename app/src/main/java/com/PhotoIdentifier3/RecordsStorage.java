package com.PhotoIdentifier3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.sql.Blob;

public class RecordsStorage {
    private static final String CLIENT_NUMBER = "MobNo";
    private static final String CLIENT_TABLE = "Client";
    private static final String DB_NAME = "PhotoIdentifier";
    private static final int DB_VERSION = 1;
    private static final String KEY_PATIENT_ADDRESS = "PatientAddress";
    private static final String KEY_PATIENT_AGE = "PatientAge";
    private static final String KEY_PATIENT_DATE = "PatientDate";
    private static final String KEY_PATIENT_FATHER = "PatientFather";
    private static final String KEY_PATIENT_IMAGE = "PatientImage";
    private static final String KEY_PATIENT_LATITUDE = "PatientLatitude";
    private static final String KEY_PATIENT_LONGITUDE = "PatientLongitude";
    private static final String KEY_PATIENT_NAME = "patientName";
    private static final String KEY_PATIENT_NO = "patientNo";
    private static final String KEY_PATIENT_TBNO = "PatientTBno";
    private static final String LATLONG_TABLE = "LATLONG";
    private static final String RECORDS_TABLE = "PatientDetails";
    public static final int STATE_NOT_SYNCED = 0;
    public static final int STATE_SYNCED = 1;

    /* renamed from: cu */
    Cursor f76cu;
    private Context mContext = null;
    private SQLiteDatabase mDatabase = null;
    private DatabaseHelper mHelper = null;

    private class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context) {
            super(context, RecordsStorage.DB_NAME, null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS PatientDetails(_id integer primary key autoincrement, PatientLatitude Long not null,PatientLongitude Long not null,PatientImage blob);");
            db.execSQL("CREATE TABLE IF NOT EXISTS LATLONG(uid integer primary key autoincrement, PatientDate Long not null,PatientLatitude Long not null,PatientLongitude Long not null);");
            db.execSQL("CREATE TABLE IF NOT EXISTS Client(uid integer primary key autoincrement, MobNo integer not null);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public RecordsStorage() {
    }

    public RecordsStorage(Context context) {
        this.mContext = context;
        this.mHelper = new DatabaseHelper(this.mContext);
        this.mDatabase = this.mHelper.getWritableDatabase();
    }

    public void insertlatlongtime(Long lat, Long lon, Long time) {
        ContentValues values = new ContentValues();
        values.put(KEY_PATIENT_LONGITUDE, lon);
        values.put(KEY_PATIENT_LATITUDE, lat);
        values.put(KEY_PATIENT_DATE, time);
        this.mDatabase.insert(LATLONG_TABLE, null, values);
    }

    public void insertPatient(byte[] image, Long lat, Long lon) {
        ContentValues values = new ContentValues();
        values.put(KEY_PATIENT_IMAGE, image);
        values.put(KEY_PATIENT_LONGITUDE, lon);
        values.put(KEY_PATIENT_LATITUDE, lat);
        this.mDatabase.insert(RECORDS_TABLE, null, values);
    }

    public void updatePatient(String refer, String name, String address, Blob image, String tb, String pdrug, String lfm, String pm, String date) {
        ContentValues values = new ContentValues();
        values.put(KEY_PATIENT_NAME, name);
        values.put(KEY_PATIENT_ADDRESS, address);
        values.put(KEY_PATIENT_TBNO, tb);
        values.put(KEY_PATIENT_DATE, date);
        this.mDatabase.update(RECORDS_TABLE, values, "patientNo =?", new String[]{refer});
    }

    public String getNo() {
        Cursor cursor = this.mDatabase.rawQuery("SELECT MobNo FROM Client", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public void removePatientdetails(String h, boolean t) {
    }

    public String getPatientNo(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT patientNo FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientName(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT patientName FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientAddress(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientAddress FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientTBno(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientTBno FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientCategory(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientCategory FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientPresentDrug(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientPresentDrug FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientLastMonth(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientLastMonth FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientPresentMonth(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientPresentMonth FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientDate(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientDate FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public String getPatientFollowUp(String w) {
        Cursor cursor = this.mDatabase.rawQuery("SELECT PatientFollowUp FROM PatientDetails Where patientNo='" + w + "'", null);
        cursor.moveToFirst();
        return cursor.getString(0);
    }

    public void clearTable() {
        this.mDatabase.delete(RECORDS_TABLE, null, null);
    }

    public void closeDatabase() {
        if (this.mDatabase != null) {
            this.mDatabase.close();
            this.mDatabase = null;
        }
        if (this.mHelper != null) {
            this.mHelper.close();
            this.mHelper = null;
        }
    }

    public int getRecordsCount() {
        return this.mDatabase.rawQuery("SELECT * FROM PatientDetails", null).getCount();
    }

    public int getSumByKey(String key) {
        int count = 0;
        Cursor cursor = this.mDatabase.rawQuery("SELECT SUM( " + key + ") FROM " + RECORDS_TABLE, null);
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return count;
    }

    public int getMaxByKey(String key) {
        int count = 0;
        Cursor cursor = this.mDatabase.rawQuery("SELECT MAX( " + key + ") FROM " + RECORDS_TABLE, null);
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return count;
    }

    public int getMINByKey(String key) {
        int count = 0;
        Cursor cursor = this.mDatabase.rawQuery("SELECT MIN( " + key + ") FROM " + RECORDS_TABLE, null);
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return count;
    }

    public int getAvgByKey(String key) {
        int count = 0;
        Cursor cursor = this.mDatabase.rawQuery("SELECT AVG( " + key + ") FROM " + RECORDS_TABLE, null);
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return count;
    }
}
