package com.PhotoIdentifier3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class CamDemo extends Activity implements Callback {
    private static final int CAMERA_PIC_REQUEST = 1337;
    private static final String OPERATION_NAME = "BinCapturing";
    protected static final String PHOTO_TAKEN = "photo_taken";
    private static final String SOAP_ACTION = "http://www.ramky.com/BinCapturing";
    private static final String WSDL_TARGET_NAMESPACE = "http://www.ramky.com/";
    public static boolean click = true;
    private static int hgt = 0;

    /* renamed from: i1 */
    public static ImageView f1i1;
    public static boolean latlon = false;
    private static ProgressDialog mProgressDialog = null;

    /* renamed from: t1 */
    public static TextView f2t1;
    private static int wid = 0;
    protected String _path;

    /* renamed from: b */
    Button f3b;
    Bitmap bitmap;
    /* access modifiers changed from: private */
    public Camera camera = null;
    /* access modifiers changed from: private */
    public SurfaceHolder cameraSurfaceHolder = null;
    /* access modifiers changed from: private */
    public SurfaceView cameraSurfaceView = null;
    private Display display = null;
    ImageView edate;
    boolean excep = false;
    Intent intent;
    final PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                CamDemo.this.f5t = data;
                CamDemo.this.task = new Fetcher();
                CamDemo.this.task.execute(new Void[0]);
            } catch (Exception e) {
            }
        }
    };
    LocationManager locManager;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            CamDemo.this.updateWithNewLocation(location);
        }

        public void onProviderDisabled(String provider) {
            CamDemo.this.updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    private boolean previewing = false;

    /* renamed from: r */
    public RecordsStorage f4r;
    final PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };
    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        }
    };

    /* renamed from: t */
    byte[] f5t;
    /* access modifiers changed from: private */
    public Fetcher task;
    private Fetcher1 task1;
    long time;

    public class ButtonClickHandler implements OnClickListener {
        public ButtonClickHandler() {
        }

        public void onClick(View view) {
            CamDemo.this.startCameraActivity();
        }
    }

    class Fetcher extends AsyncTask<Void, List<Void>, Void> {
        Fetcher() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CamDemo.this.showProgressDialog("Please wait while processing..");
            new CountDownTimer(10000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    try {
                        if (CamDemo.latlon) {
                            CamDemo.hideProgressDialog();
                            CamDemo.latlon = false;
                        } else {
                            Newservice.lat = 0.0d;
                            Newservice.lon = 0.0d;
                            try {
                                new RecordsStorage(CamDemo.this).insertPatient(CamDemo.this.f5t, Long.valueOf((long) Newservice.lat), Long.valueOf((long) Newservice.lon));
                            } catch (Exception e) {
                            }
                            for (int u = 0; u < 1; u++) {
                                Toast.makeText(CamDemo.this, "Unable to get GPS Location.Please try again", 1).show();
                            }
                            CamDemo.hideProgressDialog();
                        }
                        CamDemo.hideProgressDialog();
                        try {
                            if (CamDemo.this.camera != null) {
                                CamDemo.this.camera.stopPreview();
                                CamDemo.this.camera.release();
                                CamDemo.this.camera = null;
                            }
                            try {
                                CamDemo.this.setContentView(C0033R.layout.homescreen);
                                CamDemo.this.cameraSurfaceView = (SurfaceView) CamDemo.this.findViewById(C0033R.C0034id.cameraSurfaceView);
                                CamDemo.this.cameraSurfaceHolder = CamDemo.this.cameraSurfaceView.getHolder();
                                CamDemo.this.cameraSurfaceHolder.addCallback(CamDemo.this);
                                CamDemo.this.cameraSurfaceHolder.setType(3);
                            } catch (Exception t) {
                                t.printStackTrace();
                            }
                        } catch (Exception e2) {
                        }
                        CamDemo.click = true;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        CamDemo.hideProgressDialog();
                        try {
                            if (CamDemo.this.camera != null) {
                                CamDemo.this.camera.stopPreview();
                                CamDemo.this.camera.release();
                                CamDemo.this.camera = null;
                            }
                            try {
                                CamDemo.this.setContentView(C0033R.layout.homescreen);
                                CamDemo.this.cameraSurfaceView = (SurfaceView) CamDemo.this.findViewById(C0033R.C0034id.cameraSurfaceView);
                                CamDemo.this.cameraSurfaceHolder = CamDemo.this.cameraSurfaceView.getHolder();
                                CamDemo.this.cameraSurfaceHolder.addCallback(CamDemo.this);
                                CamDemo.this.cameraSurfaceHolder.setType(3);
                            } catch (Exception t2) {
                                t2.printStackTrace();
                            }
                        } catch (Exception e3) {
                        }
                        CamDemo.click = true;
                        try {
                            throw th2;
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }
                }
            }.start();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void notUsed) {
            CamDemo.hideProgressDialog();
            try {
                if (!CamDemo.this.excep) {
                    CamDemo.latlon = true;
                    CamDemo.latlon = false;
                    for (int y = 0; y < 2; y++) {
                        Toast.makeText(CamDemo.this, "Values inserted Sucessfully", 1).show();
                    }
                } else {
                    try {
                        new RecordsStorage(CamDemo.this).insertPatient(CamDemo.this.f5t, Long.valueOf((long) Newservice.lat), Long.valueOf((long) Newservice.lon));
                    } catch (Exception e) {
                    }
                    for (int y2 = 0; y2 < 2; y2++) {
                        Toast.makeText(CamDemo.this, "Error in  Network please try again later", 1).show();
                    }
                }
                CamDemo.hideProgressDialog();
                try {
                    if (CamDemo.this.camera != null) {
                        CamDemo.this.camera.stopPreview();
                        CamDemo.this.camera.release();
                        CamDemo.this.camera = null;
                    }
                    try {
                        CamDemo.this.setContentView(C0033R.layout.homescreen);
                        CamDemo.this.cameraSurfaceView = (SurfaceView) CamDemo.this.findViewById(C0033R.C0034id.cameraSurfaceView);
                        CamDemo.this.cameraSurfaceHolder = CamDemo.this.cameraSurfaceView.getHolder();
                        CamDemo.this.cameraSurfaceHolder.addCallback(CamDemo.this);
                        CamDemo.this.cameraSurfaceHolder.setType(3);
                    } catch (Exception t) {
                        t.printStackTrace();
                    }
                } catch (Exception e2) {
                }
                CamDemo.click = true;
            } catch (Throwable th) {
                Throwable th2 = th;
                CamDemo.hideProgressDialog();
                try {
                    if (CamDemo.this.camera != null) {
                        CamDemo.this.camera.stopPreview();
                        CamDemo.this.camera.release();
                        CamDemo.this.camera = null;
                    }
                    try {
                        CamDemo.this.setContentView(C0033R.layout.homescreen);
                        CamDemo.this.cameraSurfaceView = (SurfaceView) CamDemo.this.findViewById(C0033R.C0034id.cameraSurfaceView);
                        CamDemo.this.cameraSurfaceHolder = CamDemo.this.cameraSurfaceView.getHolder();
                        CamDemo.this.cameraSurfaceHolder.addCallback(CamDemo.this);
                        CamDemo.this.cameraSurfaceHolder.setType(3);
                    } catch (Exception t2) {
                        t2.printStackTrace();
                    }
                } catch (Exception e3) {
                }
                CamDemo.click = true;
                try {
                    throw th2;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            do {
                try {
                    if (Newservice.lat >= 0.0d) {
                        if (Newservice.lat == 0.0d) {
                            Newservice.lat = -1.0d;
                            Newservice.lon = -1.0d;
                            System.out.println("INSIDEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
                        }
                        TelephonyManager telephonyManager = (TelephonyManager) CamDemo.this.getSystemService("phone");
                        SoapObject request = new SoapObject(CamDemo.WSDL_TARGET_NAMESPACE, CamDemo.OPERATION_NAME);
                        request.addProperty("IME_number", (Object) telephonyManager.getDeviceId());
                        request.addProperty("Lon_value", (Object) Double.toString(Newservice.lon));
                        request.addProperty("Lat_value", (Object) Double.toString(Newservice.lat));
                        request.addProperty("BinImg", (Object) Base64.encode(CamDemo.this.f5t));
                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                        envelope.dotNet = true;
                        envelope.encodingStyle = SoapEnvelope.ENC2003;
                        envelope.encodingStyle = SoapEnvelope.XSD;
                        envelope.setOutputSoapObject(request);
                        HttpTransportSE httpTransport = new HttpTransportSE("http://34.93.133.27:8181/vtsws/binlocationservice1.asmx");
                        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                        httpTransport.call(CamDemo.SOAP_ACTION, envelope);
                        System.out.println((SoapObject) envelope.bodyIn);
                    }
                } catch (Exception tt) {
                    try {
                        CamDemo.this.excep = true;
                        System.out.println("Iam at tt");
                        tt.printStackTrace();
                    } catch (Exception e) {
                    }
                }
            } while (!CamDemo.latlon);
            return null;
        }
    }

    class Fetcher1 extends AsyncTask<Void, List<Void>, Void> {

        /* renamed from: DB */
        SQLiteDatabase f6DB;

        /* renamed from: c */
        Cursor f7c;

        /* renamed from: d */
        Cursor f8d;
        boolean gott = true;

        /* renamed from: m */
        Cursor f9m;

        Fetcher1() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CamDemo.this.showProgressDialog("Please wait while Syncing Records..");
            this.f6DB = CamDemo.this.openOrCreateDatabase("PhotoIdentifier", 0, null);
            this.f7c = this.f6DB.rawQuery("SELECT PatientLatitude FROM PatientDetails ", null);
            this.f9m = this.f6DB.rawQuery("SELECT PatientLongitude FROM PatientDetails", null);
            this.f8d = this.f6DB.rawQuery("SELECT PatientImage FROM PatientDetails", null);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void notUsed) {
            CamDemo.hideProgressDialog();
            if (this.gott) {
                Toast.makeText(CamDemo.this, "Sync SuccessFull", 1).show();
            } else {
                Toast.makeText(CamDemo.this, "Not Synced,tRy again later", 1).show();
            }
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                this.f7c.moveToLast();
                this.f9m.moveToLast();
                this.f8d.moveToLast();
                do {
                    TelephonyManager telephonyManager = (TelephonyManager) CamDemo.this.getSystemService("phone");
                    SoapObject request = new SoapObject(CamDemo.WSDL_TARGET_NAMESPACE, CamDemo.OPERATION_NAME);
                    request.addProperty("IME_number", (Object) telephonyManager.getDeviceId());
                    request.addProperty("Lon_value", (Object) Long.valueOf(this.f9m.getLong(0)));
                    request.addProperty("Lat_value", (Object) Long.valueOf(this.f7c.getLong(0)));
                    request.addProperty("BinImg", (Object) Base64.encode(this.f8d.getBlob(0)));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.encodingStyle = SoapEnvelope.ENC2003;
                    envelope.encodingStyle = SoapEnvelope.XSD;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE httpTransport = new HttpTransportSE("http://34.93.133.27:8181/vtsws/binlocationservice1.asmx");
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    httpTransport.call(CamDemo.SOAP_ACTION, envelope);
                    System.out.println((SoapObject) envelope.bodyIn);
                    if (!this.f7c.moveToPrevious() || !this.f8d.moveToPrevious()) {
                        this.f7c.close();
                        this.f8d.close();
                        this.f9m.close();
                        this.f6DB.close();
                        new RecordsStorage(CamDemo.this).clearTable();
                    }
                } while (this.f9m.moveToPrevious());
                this.f7c.close();
                this.f8d.close();
                this.f9m.close();
                this.f6DB.close();
                new RecordsStorage(CamDemo.this).clearTable();
            } catch (Exception e) {
                this.gott = false;
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, Newservice.class));
        setRequestedOrientation(0);
        this.display = getWindowManager().getDefaultDisplay();
        wid = this.display.getWidth();
        hgt = this.display.getHeight();
        getWindow().setFormat(-3);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this._path = Environment.getExternalStorageDirectory() + File.separator + String.format("%d.JPEG", new Object[]{Long.valueOf(System.currentTimeMillis())});
        this.locManager = (LocationManager) getSystemService("location");
        this.locManager.requestLocationUpdates("gps", 1000, 500.0f, this.locationListener);
        Location location = this.locManager.getLastKnownLocation("gps");
        if (location != null) {
            this.time = location.getTime();
        }
        try {
            if (this.camera != null) {
                this.camera.stopPreview();
                this.camera.release();
                this.camera = null;
            }
            try {
                setContentView(C0033R.layout.homescreen);
                this.cameraSurfaceView = (SurfaceView) findViewById(C0033R.C0034id.cameraSurfaceView);
                this.cameraSurfaceHolder = this.cameraSurfaceView.getHolder();
                this.cameraSurfaceHolder.addCallback(this);
                this.cameraSurfaceHolder.setType(3);
            } catch (Exception t) {
                t.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.previewing) {
            this.camera.stopPreview();
            this.previewing = false;
        }
        if (this.camera != null) {
            try {
                this.camera.setPreviewDisplay(this.cameraSurfaceHolder);
                this.camera.startPreview();
                this.previewing = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.camera = Camera.open();
        } catch (RuntimeException e) {
            Toast.makeText(getApplicationContext(), "Device camera is not working properly, please try after sometime.", 1).show();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.camera.stopPreview();
        this.camera.release();
        this.camera = null;
        this.previewing = false;
    }

    private byte[] bitmaptoByteArray(Bitmap bitmap2) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap2.compress(CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    /* access modifiers changed from: protected */
    public void startCameraActivity() {
        Uri outputFileUri = Uri.fromFile(new File(this._path));
        this.intent = new Intent("android.media.action.IMAGE_CAPTURE");
        this.intent.putExtra("output", outputFileUri);
        System.out.println("dsfGsdfgsdfgdfg");
        startActivityForResult(this.intent, CAMERA_PIC_REQUEST);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println(requestCode);
        System.out.println(resultCode);
        switch (resultCode) {
            case -1:
                onPhotoTaken();
                return;
            case 0:
                finish();
                return;
            case 24:
                onPhotoTaken();
                return;
            case 25:
                onPhotoTaken();
                return;
            case 82:
                onPhotoTaken();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.out.println("PAUSEJAAJSJF");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onPause();
        System.out.println("****************");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (keyCode != 4) {
            if (keyCode == 25) {
                if (click) {
                    click = false;
                    this.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
                }
            } else if (keyCode == 24) {
                if (click) {
                    click = false;
                    this.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
                }
            } else if (keyCode == 82) {
                openOptionsMenu();
            }
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, "Sync");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 0:
                if (new RecordsStorage(this).getRecordsCount() <= 0) {
                    Toast.makeText(this, "No records to sync", 1).show();
                    break;
                } else {
                    this.task1 = new Fetcher1();
                    this.task1.execute(new Void[0]);
                    break;
                }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPhotoTaken() {
        Options options = new Options();
        options.inSampleSize = 4;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16384];
        this.bitmap = BitmapFactory.decodeFile(this._path, options);
        System.out.println(this._path);
        System.out.println(this.bitmap);
    }

    /* access modifiers changed from: private */
    public void updateWithNewLocation(Location location) {
        if (location != null) {
            Newservice.lat = location.getLatitude();
            Newservice.lon = location.getLongitude();
            latlon = true;
        }
    }

    public void showProgressDialog(String message) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void showProgressDialog(Context context, String message) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void showProgressDialog(String message, OnKeyListener listener) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setOnKeyListener(listener);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void setProgressMessage(String message) {
        if (mProgressDialog != null) {
            mProgressDialog.setMessage(message);
        } else {
            showProgressDialog(message);
        }
    }
}
