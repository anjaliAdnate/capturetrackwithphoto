package com.PhotoIdentifier3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;
import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.p003v1.XmlPullParser;

public class TransCls extends Activity {
    /* access modifiers changed from: private */
    public static String OPERATION_NAME = null;
    /* access modifiers changed from: private */
    public static String SOAP_ACTION = null;
    private static final String SOAP_ADDRESS = "http://34.93.133.27:8181/vtsws/binlocationservice1.asmx";
    private static final int TAKE_PICTURE = 0;
    private static final String WSDL_TARGET_NAMESPACE = "http://www.ramky.com/";
    String IMEIno = XmlPullParser.NO_NAMESPACE;
    Bitmap ImgBitmap = null;
    String _path;
    double latitude = 0.0d;
    LocationManager locationManager;
    double longitude = 0.0d;
    LocationListener mlocListener;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog = null;
    List<String> providers;
    SoapObject request;
    Object result;
    SoapObject resultGet;
    String strBase64 = XmlPullParser.NO_NAMESPACE;

    /* renamed from: t */
    byte[] f77t;

    public class MyLocationListener implements LocationListener {
        public MyLocationListener() {
        }

        public void onLocationChanged(Location loc) {
            TransCls.this.latitude = loc.getLatitude();
            TransCls.this.longitude = loc.getLongitude();
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }

    class UploadInfo extends AsyncTask<String, String, String> {
        UploadInfo() {
        }

        public void onPreExecute() {
            TransCls.this.progressDialog = new ProgressDialog(TransCls.this);
            TransCls.this.progressDialog.setMessage("Please wait while uploading..");
            TransCls.this.progressDialog.setProgressStyle(0);
            TransCls.this.progressDialog.setCancelable(false);
            TransCls.this.progressDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            TransCls.SOAP_ACTION = "http://www.ramky.com/BinCapturing";
            TransCls.OPERATION_NAME = "BinCapturing";
            TransCls.this.request = new SoapObject(TransCls.WSDL_TARGET_NAMESPACE, TransCls.OPERATION_NAME);
            try {
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                TransCls.this.ImgBitmap.compress(CompressFormat.JPEG, 60, bao);
                TransCls.this.f77t = bao.toByteArray();
                TransCls.this.strBase64 = Base64.encode(TransCls.this.f77t);
            } catch (Exception e) {
                e.printStackTrace();
            }
            TelephonyManager telephonyManager = (TelephonyManager) TransCls.this.getSystemService("phone");
            TransCls.this.IMEIno = telephonyManager.getDeviceId();
            TransCls.this.request.addProperty("IME_number", (Object) TransCls.this.IMEIno.trim());
            TransCls.this.request.addProperty("Lat_value", (Object) Double.toString(TransCls.this.latitude));
            TransCls.this.request.addProperty("Lon_value", (Object) Double.toString(TransCls.this.longitude));
            TransCls.this.request.addProperty("BinImg", (Object) TransCls.this.strBase64.trim());
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(TransCls.this.request);
            try {
                new HttpTransportSE(TransCls.SOAP_ADDRESS).call(TransCls.SOAP_ACTION, envelope);
                TransCls.this.result = envelope.getResponse();
                Log.i("request result", TransCls.this.result.toString());
            } catch (Exception exception) {
                exception.toString();
            }
            TransCls.this.progressDialog.dismiss();
            return null;
        }

        public void onPostExecute(String params) {
            TransCls.this.progressDialog.dismiss();
            try {
                String requestResult = TransCls.this.result.toString();
                if (requestResult.equalsIgnoreCase("ok")) {
                    Toast.makeText(TransCls.this, "Successfully uploaded ...", 1).show();
                    TransCls.this.finish();
                    TransCls.this.startActivity(new Intent(TransCls.this, TransCls.class));
                    return;
                }
                Toast.makeText(TransCls.this, "Please try again..." + requestResult, 1).show();
                TransCls.this.finish();
            } catch (NullPointerException e) {
                Toast.makeText(TransCls.this, "Please check your internet connection..", 1).show();
                TransCls.this.finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, Newservice.class));
        this._path = Environment.getExternalStorageDirectory() + File.separator + String.format("%d.JPEG", new Object[]{Long.valueOf(System.currentTimeMillis())});
        onTakePic();
    }

    private void onTakePic() {
        Uri outputFileUri = Uri.fromFile(new File(this._path));
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", outputFileUri);
        System.out.println("dsfGsdfgsdfgdfg");
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            Options options = new Options();
            options.inSampleSize = 2;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16384];
            this.ImgBitmap = BitmapFactory.decodeFile(this._path, options);
            currentLocation();
            new UploadInfo().execute(new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        System.out.println("PAUSEJAAJSJF");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onPause();
        System.out.println("****************");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (keyCode == 4) {
            finish();
        } else if (keyCode == 25) {
            currentLocation();
            new UploadInfo().execute(new String[0]);
        } else if (keyCode == 24) {
            currentLocation();
            new UploadInfo().execute(new String[0]);
        }
        return true;
    }

    public void currentLocation() {
        this.locationManager = (LocationManager) getSystemService("location");
        this.providers = this.locationManager.getProviders(true);
        this.mlocListener = new MyLocationListener();
        this.locationManager.requestLocationUpdates("gps", 0, 0.0f, this.mlocListener);
        System.out.println("providers size" + this.providers.size());
        for (int i = 0; i < this.providers.size(); i++) {
            System.out.println("providers " + ((String) this.providers.get(i)));
        }
        Location location = null;
        for (int i2 = this.providers.size() - 1; i2 >= 0; i2--) {
            location = this.locationManager.getLastKnownLocation((String) this.providers.get(i2));
            if (location != null) {
                break;
            }
        }
        if (location == null) {
            System.out.print("location null");
        }
        if (location != null) {
            this.latitude = 0.0d;
            this.longitude = 0.0d;
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
