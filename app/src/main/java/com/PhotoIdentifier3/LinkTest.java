package com.PhotoIdentifier3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class LinkTest extends Activity {
    /* access modifiers changed from: private */
    public static String OPERATION_NAME = null;
    /* access modifiers changed from: private */
    public static String SOAP_ACTION = null;
    private static final String SOAP_ADDRESS = "http://34.93.133.27:8181/BTSWCFService/BintransactionService.asmx?WSDL";
    private static final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    Button btnLinkTest;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    SoapObject request;
    Object result;

    class GetBeniInfo extends AsyncTask<String, String, String> {
        GetBeniInfo() {
        }

        public void onPreExecute() {
            LinkTest.this.progressDialog = new ProgressDialog(LinkTest.this);
            LinkTest.this.progressDialog.setMessage("Loading..");
            LinkTest.this.progressDialog.setProgressStyle(0);
            LinkTest.this.progressDialog.setCancelable(false);
            LinkTest.this.progressDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            LinkTest.SOAP_ACTION = "http://tempuri.org/HelloWorld";
            LinkTest.OPERATION_NAME = "HelloWorld";
            LinkTest.this.request = new SoapObject(LinkTest.WSDL_TARGET_NAMESPACE, LinkTest.OPERATION_NAME);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(LinkTest.this.request);
            try {
                new HttpTransportSE(LinkTest.SOAP_ADDRESS).call(LinkTest.SOAP_ACTION, envelope);
                LinkTest.this.result = envelope.getResponse();
                Log.i("request result", LinkTest.this.result.toString());
            } catch (Exception exception) {
                Log.i("request result", exception.toString());
            }
            LinkTest.this.progressDialog.dismiss();
            return null;
        }

        public void onPostExecute(String params) {
            LinkTest.this.progressDialog.dismiss();
            try {
                if (LinkTest.this.result.toString() != null) {
                    Toast.makeText(LinkTest.this, "Success:" + LinkTest.this.result.toString(), 1).show();
                } else {
                    Toast.makeText(LinkTest.this, "Not Valid Mobile ...", 1).show();
                }
            } catch (NullPointerException e) {
                Log.i("request result", e.toString());
                Toast.makeText(LinkTest.this, "Please check your internet connection..", 1).show();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0033R.layout.link_test);
        this.btnLinkTest = (Button) findViewById(C0033R.C0034id.btn_link_test);
        this.btnLinkTest.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new GetBeniInfo().execute(new String[0]);
            }
        });
    }
}
