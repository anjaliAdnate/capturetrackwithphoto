package com.PhotoIdentifier3;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import org.xmlpull.p003v1.XmlPullParser;

public class Display extends ListActivity {
    static ArrayList<HashMap<String, Object>> list4 = new ArrayList<>();

    /* renamed from: DB */
    SQLiteDatabase f28DB;
    HashMap<Object, String> Position = new HashMap<>();

    /* renamed from: c */
    Cursor f29c;
    Cursor content;

    /* renamed from: d */
    Cursor f30d;
    Dialog dialog;

    /* renamed from: e */
    Cursor f31e;

    /* renamed from: i */
    int f32i = 0;

    /* renamed from: id */
    Cursor f33id;

    /* renamed from: j */
    Cursor f34j;

    /* renamed from: k */
    Cursor f35k;

    /* renamed from: l */
    Cursor f36l;

    /* renamed from: lv */
    ListView f37lv;

    /* renamed from: m */
    Cursor f38m;

    /* renamed from: n */
    Cursor f39n;
    private SimpleAdapter notes;

    /* renamed from: q */
    Cursor f40q;

    /* renamed from: t */
    int f41t;

    /* renamed from: t1 */
    TextView f42t1;

    /* renamed from: t6 */
    TextView f43t6;

    /* renamed from: t8 */
    TextView f44t8;

    /* renamed from: v */
    View f45v;

    /* renamed from: y */
    int f46y;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.gc();
        requestWindowFeature(1);
        setContentView(C0033R.layout.listall);
        this.f42t1 = (TextView) findViewById(C0033R.C0034id.textView1);
        this.f37lv = (ListView) findViewById(16908298);
        list4.clear();
        this.f43t6 = (TextView) findViewById(C0033R.C0034id.textView6);
        this.f44t8 = (TextView) findViewById(C0033R.C0034id.textView8);
        try {
            this.f28DB = openOrCreateDatabase("PhotoIdentifier", 0, null);
            this.f33id = this.f28DB.rawQuery("SELECT patientNo FROM PatientDetails", null);
            this.f29c = this.f28DB.rawQuery("SELECT PatientLatitude FROM PatientDetails ", null);
            this.f34j = this.f28DB.rawQuery("SELECT patientName FROM PatientDetails ", null);
            this.f35k = this.f28DB.rawQuery("SELECT PatientAddress FROM PatientDetails", null);
            this.f36l = this.f28DB.rawQuery("SELECT PatientDate FROM PatientDetails", null);
            this.f38m = this.f28DB.rawQuery("SELECT PatientLongitude FROM PatientDetails", null);
            this.f30d = this.f28DB.rawQuery("SELECT PatientImage FROM PatientDetails", null);
            this.f31e = this.f28DB.rawQuery("SELECT PatientAge FROM PatientDetails", null);
            this.f40q = this.f28DB.rawQuery("SELECT PatientTBno FROM PatientDetails", null);
            this.f41t = this.f33id.getCount();
            this.f37lv.setEnabled(true);
            this.f37lv.requestFocusFromTouch();
            if (this.f33id.getCount() > 0) {
                this.f33id.moveToLast();
                this.f29c.moveToLast();
                this.f34j.moveToLast();
                this.f35k.moveToLast();
                this.f36l.moveToLast();
                this.f38m.moveToLast();
                this.f30d.moveToLast();
                this.f31e.moveToLast();
                this.f40q.moveToLast();
                do {
                    HashMap<String, Object> item = new HashMap<>();
                    item.put("line1", "DTC NO: ".concat(this.f33id.getString(0).toString().trim()));
                    item.put("line2", this.f29c.getString(0).toString().trim());
                    item.put("line3", "Patient Name: ".concat(this.f34j.getString(0).toString().trim()));
                    item.put("line4", "Patient Address: ".concat(this.f35k.getString(0).toString().trim()));
                    item.put("line5", this.f36l.getString(0).toString().trim());
                    item.put("line6", this.f38m.getString(0).toString().trim());
                    item.put("line7", this.f31e.getString(0).toString().trim());
                    item.put("line8", this.f40q.getString(0).toString().trim());
                    Bitmap decodeStream = BitmapFactory.decodeStream(new ByteArrayInputStream(this.f30d.getBlob(0)));
                    item.put("key_for_image", Integer.valueOf(C0033R.drawable.icon));
                    list4.add(item);
                    if (!this.f40q.moveToPrevious() || !this.f29c.moveToPrevious() || !this.f34j.moveToPrevious() || !this.f35k.moveToPrevious() || !this.f36l.moveToPrevious()) {
                        this.f29c.close();
                        this.f30d.close();
                        this.f31e.close();
                        this.f34j.close();
                        this.f35k.close();
                        this.f36l.close();
                        this.f33id.close();
                        this.f40q.close();
                        this.f28DB.close();
                    }
                } while (this.f33id.moveToPrevious());
                this.f29c.close();
                this.f30d.close();
                this.f31e.close();
                this.f34j.close();
                this.f35k.close();
                this.f36l.close();
                this.f33id.close();
                this.f40q.close();
                this.f28DB.close();
            } else {
                Toast toast = Toast.makeText(getBaseContext(), "Empty list", 0);
                toast.setGravity(17, 0, 0);
                toast.show();
            }
            this.notes = new SimpleAdapter(this, list4, C0033R.layout.listview, new String[]{"line1", "line3", "line4"}, new int[]{C0033R.C0034id.textView6, C0033R.C0034id.textView5, C0033R.C0034id.textView7});
            setListAdapter(this.notes);
        } catch (Exception e) {
            Toast toast2 = Toast.makeText(getBaseContext(), "Empty list", 0);
            toast2.setGravity(17, 0, 0);
            toast2.show();
        }
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        super.onListItemClick(parent, v, position, id);
        Log.i(XmlPullParser.NO_NAMESPACE, "HELLO YOU CLICKED ME" + position + ((String) this.Position.get(Integer.valueOf(position))));
        this.f28DB.close();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            Intent k = new Intent(this, PhotoIdentifierActivity.class);
            finish();
            startActivity(k);
        } else if (keyCode == 82) {
            openOptionsMenu();
        } else if (keyCode == 3) {
            return false;
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        item.getItemId();
        return true;
    }
}
