package com.PhotoIdentifier3;

/* renamed from: com.PhotoIdentifier3.R */
public final class C0033R {

    /* renamed from: com.PhotoIdentifier3.R$attr */
    public static final class attr {
    }

    /* renamed from: com.PhotoIdentifier3.R$drawable */
    public static final class drawable {
        public static final int bar_plain = 2130837504;
        public static final int blue = 2130837505;
        public static final int icon = 2130837506;
        public static final int icon_launcher = 2130837507;
        public static final int insert = 2130837508;
        public static final int search = 2130837509;
    }

    /* renamed from: com.PhotoIdentifier3.R$id */
    public static final class C0034id {
        public static final int ImageView01 = 2131034127;
        public static final int ImageViewbottom = 2131034129;
        public static final int ImageViewbottomright = 2131034130;
        public static final int RelativeLayout01 = 2131034126;
        public static final int RelativeLayout02 = 2131034139;
        public static final int RelativeLayout03 = 2131034138;
        public static final int btn_link_test = 2131034124;
        public static final int button = 2131034123;
        public static final int button1 = 2131034116;
        public static final int button2 = 2131034149;
        public static final int cameraSurfaceView = 2131034118;
        public static final int editText5 = 2131034144;
        public static final int editText6 = 2131034146;
        public static final int editText9 = 2131034148;
        public static final int field = 2131034122;
        public static final int imageView1 = 2131034114;
        public static final int linearLayout1 = 2131034115;
        public static final int linearLayout2 = 2131034113;
        public static final int linearLayout5 = 2131034143;
        public static final int linearLayout6 = 2131034145;
        public static final int linearLayout9 = 2131034147;
        public static final int relativeLayout1 = 2131034119;
        public static final int relativeLayout2 = 2131034120;
        public static final int relativeLayout3 = 2131034121;
        public static final int relativelayout = 2131034112;
        public static final int root = 2131034125;
        public static final int scrollView1 = 2131034142;
        public static final int surfaceView = 2131034150;
        public static final int textView1 = 2131034117;
        public static final int textView2 = 2131034131;
        public static final int textView3 = 2131034132;
        public static final int textView4 = 2131034140;
        public static final int textView5 = 2131034141;
        public static final int textView6 = 2131034133;
        public static final int textView7 = 2131034134;
        public static final int textView8 = 2131034135;
        public static final int textView9 = 2131034136;
        public static final int textViewbottom = 2131034128;
        public static final int topbutton = 2131034137;
    }

    /* renamed from: com.PhotoIdentifier3.R$layout */
    public static final class layout {
        public static final int home = 2130903040;
        public static final int homescreen = 2130903041;
        public static final int image = 2130903042;
        public static final int link_test = 2130903043;
        public static final int listall = 2130903044;
        public static final int listview = 2130903045;
        public static final int main = 2130903046;
        public static final int surfaceview = 2130903047;
    }

    /* renamed from: com.PhotoIdentifier3.R$string */
    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
