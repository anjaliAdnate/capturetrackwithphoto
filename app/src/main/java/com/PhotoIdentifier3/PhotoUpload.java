package com.PhotoIdentifier3;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;
import org.kobjects.base64.Base64;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class PhotoUpload extends Activity {
    private static final int CAMERA_PIC_REQUEST = 1337;
    /* access modifiers changed from: private */
    public static String OPERATION_NAME = null;
    protected static final String PHOTO_TAKEN = "photo_taken";
    /* access modifiers changed from: private */
    public static String SOAP_ACTION = null;
    private static final String SOAP_ADDRESS = "http://34.93.133.27:8181/dmswmws/BinLocationService1.asmx";
    private static final String WSDL_TARGET_NAMESPACE = "http://www.ramky.com/";
    public static boolean click = true;

    /* renamed from: i1 */
    public static ImageView f58i1;
    public static boolean latlon = false;
    /* access modifiers changed from: private */
    public static ProgressDialog mProgressDialog = null;

    /* renamed from: t1 */
    public static TextView f59t1;
    String IMEIno;
    protected String _path;

    /* renamed from: b */
    Button f60b;
    Bitmap bitmap;
    ImageView edate;
    boolean excep = false;
    Intent intent;
    final PictureCallback jpegCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            int orientation;
            try {
                PhotoUpload.this.localT = data;
                Options options = new Options();
                options.inSampleSize = 3;
                options.inDither = false;
                options.inPurgeable = true;
                options.inInputShareable = true;
                options.inTempStorage = new byte[32768];
                options.inPreferredConfig = Config.RGB_565;
                Bitmap myBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                if (myBitmap.getHeight() < myBitmap.getWidth()) {
                    orientation = 90;
                } else {
                    orientation = 0;
                }
                if (orientation != 0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate((float) orientation);
                    PhotoUpload.this.uploadBitmap = Bitmap.createBitmap(myBitmap, 0, 0, 650, myBitmap.getHeight(), matrix, true);
                } else {
                    Matrix mat = new Matrix();
                    mat.postRotate(180.0f);
                    PhotoUpload.this.uploadBitmap = Bitmap.createBitmap(myBitmap, 0, 0, 650, myBitmap.getHeight(), mat, true);
                }
                UploadInfo uploadInfo = new UploadInfo();
                uploadInfo.execute(new String[0]);
            } catch (Exception e) {
            }
        }
    };
    double latitude = 0.0d;
    LocationManager locManager;
    byte[] localT;
    private final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            PhotoUpload.this.updateWithNewLocation(location);
        }

        public void onProviderDisabled(String provider) {
            PhotoUpload.this.updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    double longitude = 0.0d;

    /* renamed from: r */
    public RecordsStorage f61r;
    final PictureCallback rawCallback = new PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };
    SoapObject request;
    Object result;
    ShutterCallback shutterCallback = new ShutterCallback() {
        public void onShutter() {
        }
    };

    /* renamed from: t */
    byte[] f62t;
    private Fetcher1 task1;
    long time;
    Bitmap uploadBitmap;

    public class ButtonClickHandler implements OnClickListener {
        public ButtonClickHandler() {
        }

        public void onClick(View view) {
            PhotoUpload.this.startCameraActivity();
        }
    }

    class Fetcher1 extends AsyncTask<Void, List<Void>, Void> {

        /* renamed from: DB */
        SQLiteDatabase f63DB;

        /* renamed from: c */
        Cursor f64c;

        /* renamed from: d */
        Cursor f65d;
        boolean gott = true;

        /* renamed from: m */
        Cursor f66m;

        Fetcher1() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            PhotoUpload.this.showProgressDialog("Please wait while Syncing Records..");
            this.f63DB = PhotoUpload.this.openOrCreateDatabase("PhotoIdentifier", 0, null);
            this.f64c = this.f63DB.rawQuery("SELECT PatientLatitude FROM PatientDetails ", null);
            this.f66m = this.f63DB.rawQuery("SELECT PatientLongitude FROM PatientDetails", null);
            this.f65d = this.f63DB.rawQuery("SELECT PatientImage FROM PatientDetails", null);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void notUsed) {
            PhotoUpload.hideProgressDialog();
            if (this.gott) {
                Toast.makeText(PhotoUpload.this, "Sync SuccessFull", 1).show();
            } else {
                Toast.makeText(PhotoUpload.this, "Not Synced,tRy again later", 1).show();
            }
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                this.f64c.moveToLast();
                this.f66m.moveToLast();
                this.f65d.moveToLast();
                do {
                    TelephonyManager telephonyManager = (TelephonyManager) PhotoUpload.this.getSystemService("phone");
                    SoapObject request = new SoapObject(PhotoUpload.WSDL_TARGET_NAMESPACE, PhotoUpload.OPERATION_NAME);
                    request.addProperty("IME_number", (Object) telephonyManager.getDeviceId());
                    request.addProperty("Lon_value", (Object) Long.valueOf(this.f66m.getLong(0)));
                    request.addProperty("Lat_value", (Object) Long.valueOf(this.f64c.getLong(0)));
                    request.addProperty("BinImg", (Object) Base64.encode(this.f65d.getBlob(0)));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.encodingStyle = SoapEnvelope.ENC2003;
                    envelope.encodingStyle = SoapEnvelope.XSD;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE httpTransport = new HttpTransportSE("http://34.93.133.27:8181/vtsws/binlocationservice1.asmx");
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    httpTransport.call(PhotoUpload.SOAP_ACTION, envelope);
                    System.out.println("7. " + (SoapObject) envelope.bodyIn);
                    if (!this.f64c.moveToPrevious() || !this.f65d.moveToPrevious()) {
                        this.f64c.close();
                        this.f65d.close();
                        this.f66m.close();
                        this.f63DB.close();
                        new RecordsStorage(PhotoUpload.this).clearTable();
                    }
                } while (this.f66m.moveToPrevious());
                this.f64c.close();
                this.f65d.close();
                this.f66m.close();
                this.f63DB.close();
                new RecordsStorage(PhotoUpload.this).clearTable();
            } catch (Exception e) {
                this.gott = false;
            }
            return null;
        }
    }

    class UploadInfo extends AsyncTask<String, String, String> {
        UploadInfo() {
        }

        public void onPreExecute() {
            PhotoUpload.mProgressDialog = new ProgressDialog(PhotoUpload.this);
            PhotoUpload.mProgressDialog.setMessage("Please wait while uploading..");
            PhotoUpload.mProgressDialog.setProgressStyle(0);
            PhotoUpload.mProgressDialog.setCancelable(false);
            PhotoUpload.mProgressDialog.show();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            PhotoUpload.SOAP_ACTION = "http://www.ramky.com/BinCapturing";
            PhotoUpload.OPERATION_NAME = "BinCapturing";
            PhotoUpload.this.request = new SoapObject(PhotoUpload.WSDL_TARGET_NAMESPACE, PhotoUpload.OPERATION_NAME);
            try {
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                PhotoUpload.this.uploadBitmap.compress(CompressFormat.JPEG, 70, bao);
                PhotoUpload.this.f62t = bao.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
            }
            TelephonyManager telephonyManager = (TelephonyManager) PhotoUpload.this.getSystemService("phone");
            PhotoUpload.this.IMEIno = telephonyManager.getDeviceId();
            PhotoUpload.this.request.addProperty("IME_number", (Object) PhotoUpload.this.IMEIno.trim());
            PhotoUpload.this.request.addProperty("Lon_value", (Object) Double.toString(PhotoUpload.this.longitude));
            PhotoUpload.this.request.addProperty("Lat_value", (Object) Double.toString(PhotoUpload.this.latitude));
            PhotoUpload.this.request.addProperty("BinImg", (Object) Base64.encode(PhotoUpload.this.f62t));
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(PhotoUpload.this.request);
            try {
                new HttpTransportSE(PhotoUpload.SOAP_ADDRESS).call(PhotoUpload.SOAP_ACTION, envelope);
                PhotoUpload.this.result = envelope.getResponse();
                Log.i("request result", PhotoUpload.this.result.toString());
            } catch (Exception exception) {
                exception.toString();
            }
            PhotoUpload.mProgressDialog.dismiss();
            return null;
        }

        public void onPostExecute(String params) {
            PhotoUpload.mProgressDialog.dismiss();
            try {
                String requestResult = PhotoUpload.this.result.toString();
                if (requestResult.equalsIgnoreCase("ok")) {
                    Toast.makeText(PhotoUpload.this, "Successfully uploaded ...", 1).show();
                    PhotoUpload.this.finish();
                    return;
                }
                Toast.makeText(PhotoUpload.this, "Please try again..." + requestResult, 1).show();
                PhotoUpload.this.finish();
            } catch (NullPointerException e) {
                try {
                    new RecordsStorage(PhotoUpload.this).insertPatient(PhotoUpload.this.localT, Long.valueOf((long) Newservice.lat), Long.valueOf((long) Newservice.lon));
                } catch (Exception e2) {
                }
                PhotoUpload.this.finish();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (Preview.camera != null) {
            Preview.camera.stopPreview();
            Preview.camera.release();
            Preview.camera = null;
        }
        finish();
        finish();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, Newservice.class));
        System.out.println("1. " + "#####################################3");
        this._path = Environment.getExternalStorageDirectory() + File.separator + String.format("%d.JPEG", new Object[]{Long.valueOf(System.currentTimeMillis())});
        this.locManager = (LocationManager) getSystemService("location");
        this.locManager.requestLocationUpdates("gps", 1000, 500.0f, this.locationListener);
        Location location = this.locManager.getLastKnownLocation("gps");
        if (location != null) {
            this.time = location.getTime();
        }
        try {
            if (Preview.camera != null) {
                Preview.camera.stopPreview();
                Preview.camera.release();
                Preview.camera = null;
            }
            try {
                setContentView(new Preview(this));
            } catch (Exception t) {
                t.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    private byte[] bitmaptoByteArray(Bitmap bitmap2) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap2.compress(CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    /* access modifiers changed from: protected */
    public void startCameraActivity() {
        Uri outputFileUri = Uri.fromFile(new File(this._path));
        this.intent = new Intent("android.media.action.IMAGE_CAPTURE");
        this.intent.putExtra("output", outputFileUri);
        System.out.println("2. " + "dsfGsdfgsdfgdfg");
        startActivityForResult(this.intent, CAMERA_PIC_REQUEST);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("3. " + requestCode);
        System.out.println("4. " + resultCode);
        switch (resultCode) {
            case -1:
                onPhotoTaken();
                return;
            case 0:
                finish();
                return;
            case 24:
                onPhotoTaken();
                return;
            case 25:
                onPhotoTaken();
                return;
            case 82:
                onPhotoTaken();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onPause();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (keyCode != 4) {
            if (keyCode == 25) {
                Preview.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
            } else if (keyCode == 24) {
                Preview.camera.takePicture(this.shutterCallback, this.rawCallback, this.jpegCallback);
            } else if (keyCode == 82) {
                openOptionsMenu();
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPhotoTaken() {
        Options options = new Options();
        options.inSampleSize = 4;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16384];
        this.bitmap = BitmapFactory.decodeFile(this._path, options);
        System.out.println("6. " + this._path);
        System.out.println("7. " + this.bitmap);
    }

    /* access modifiers changed from: private */
    public void updateWithNewLocation(Location location) {
        if (location != null) {
            this.latitude = location.getLatitude();
            this.longitude = location.getLongitude();
            latlon = true;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, "Sync");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 0:
                if (new RecordsStorage(this).getRecordsCount() <= 0) {
                    Toast.makeText(this, "No records to sync", 1).show();
                    break;
                } else {
                    this.task1 = new Fetcher1();
                    this.task1.execute(new Void[0]);
                    break;
                }
        }
        return true;
    }

    public void showProgressDialog(String message) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void showProgressDialog(Context context, String message) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void showProgressDialog(String message, OnKeyListener listener) {
        hideProgressDialog();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(message);
        mProgressDialog.setOnKeyListener(listener);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void setProgressMessage(String message) {
        if (mProgressDialog != null) {
            mProgressDialog.setMessage(message);
        } else {
            showProgressDialog(message);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
