package com.PhotoIdentifier3;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.List;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class Newservice extends Service implements LocationListener {
    private static final String OPERATION_NAME = "GetMobileTrackTrans";
    private static final String SOAP_ACTION = "http://tempuri.org/GetMobileTrackTrans";
    private static final String TAG = "LocationLoggerService";
    private static final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    public static double lat = 0.0d;
    public static double lon = 0.0d;

    /* renamed from: lm */
    LocationManager f47lm;

    /* renamed from: r */
    RecordsStorage f48r;
    /* access modifiers changed from: private */
    public Fetcher task;
    CountDownTimer timer;

    class Fetcher extends AsyncTask<Void, List<Void>, Void> {
        Fetcher() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void notUsed) {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (Newservice.lat > 0.0d) {
                    TelephonyManager telephonyManager = (TelephonyManager) Newservice.this.getSystemService("phone");
                    SoapObject request = new SoapObject(Newservice.WSDL_TARGET_NAMESPACE, Newservice.OPERATION_NAME);
                    request.addProperty("p_imeno", (Object) telephonyManager.getDeviceId());
                    request.addProperty("p_lattitude", (Object) Double.toString(Newservice.lat));
                    request.addProperty("p_longitude", (Object) Double.toString(Newservice.lon));
                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.encodingStyle = SoapEnvelope.ENC2003;
                    envelope.encodingStyle = SoapEnvelope.XSD;
                    envelope.setOutputSoapObject(request);
                    HttpTransportSE httpTransport = new HttpTransportSE("http://34.93.133.27:8181/BTSWCFService/BintransactionService.asmx?WSDL");
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    httpTransport.call(Newservice.SOAP_ACTION, envelope);
                    System.out.println(envelope.bodyIn);
                }
            } catch (Exception tt) {
                try {
                    tt.printStackTrace();
                } catch (Exception e) {
                }
            }
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.timer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Newservice.this.task = new Fetcher();
                Newservice.this.task.execute(new Void[0]);
                Newservice.this.timer.start();
            }
        };
        this.timer.start();
        subscribeToLocationUpdates();
    }

    public void onDestroy() {
        try {
            finalize();
            this.f47lm.removeUpdates(this);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void onLocationChanged(Location loc) {
        Log.d(TAG, loc.toString());
        PhotoIdentifierActivity.latlon = true;
        System.out.println("GOTTOIT");
        lat = loc.getLatitude();
        lon = loc.getLongitude();
        System.out.println(loc.getLatitude());
        System.out.println(loc.getLongitude());
        System.out.println(loc.getTime());
    }

    public void onProviderEnabled(String s) {
    }

    public void onProviderDisabled(String s) {
    }

    public void onStatusChanged(String s, int i, Bundle b) {
    }

    public void subscribeToLocationUpdates() {
        this.f47lm = (LocationManager) getSystemService("location");
        this.f47lm.requestLocationUpdates("gps", 30000, 0.0f, this);
    }
}
